package reddit.garmash.viewcontrollers.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import reddit.garmash.core.BaseActivity
import reddit.garmash.viewcontrollers.main.MainActivity
import reddit.garmash.viewcontrollers.main.R
import reddit.garmash.viewcontrollers.splash.presenter.SplashScreenPresenterImpl
import reddit.garmash.viewcontrollers.splash.view.SplashScreenView

class SplashScreen : BaseActivity(), SplashScreenView {


    var splashPresenter: SplashScreenPresenterImpl? = null;
    private var handler: android.os.Handler? = null
    private val runnable = Runnable { startActivity(Intent(this, MainActivity::class.java)) }
    private val START_DELAY = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.splash_screen)
        handler = Handler()
        splashPresenter = SplashScreenPresenterImpl(this)
        splashPresenter?.onCreate()
    }


    override fun openNextScreen() {
        handler?.postDelayed(runnable, START_DELAY.toLong())
    }


    override fun onDestroy() {
        super.onDestroy()
        splashPresenter?.onDestroy()

    }

}
