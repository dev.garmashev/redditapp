package reddit.garmash.viewcontrollers.main.repository

import android.support.annotation.NonNull
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import reddit.garmash.api.ApiManager
import reddit.garmash.api.CallbackWrapper
import reddit.garmash.core.BaseView
import reddit.garmash.viewcontrollers.main.models.RedditPost
import reddit.garmash.viewcontrollers.main.models.RedditResponse

class RedditPostsRepositoryImpl (val baseView : BaseView): RedditPostsRepository {

    override fun getRedditPosts(loadSize: Int, nextLoadKey: String?, @NonNull onLoadComplete: OnLoadComplete) {

      ApiManager.getInstance().service.getTop(nextLoadKey, loadSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :CallbackWrapper<RedditResponse>(baseView){
                    override fun onSuccess(response: RedditResponse?) {
                    onLoadComplete.onLoadComplete(response?.redditPosts, response?.redditData?.nextLoadKey)
                    }
                })
    }

    interface OnLoadComplete {

        fun onLoadComplete(redditPosts: List<RedditPost>?, nextLoadKey : String?)

    }
}
