package reddit.garmash.viewcontrollers.main

import android.arch.paging.PagedList
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import reddit.garmash.core.BaseActivity
import reddit.garmash.utils.Const
import reddit.garmash.viewcontrollers.main.adapter.MainPostAdapter
import reddit.garmash.viewcontrollers.main.adapter.PostDiffCallback
import reddit.garmash.viewcontrollers.main.models.RedditPost
import reddit.garmash.viewcontrollers.main.storage.PostsDataSource
import reddit.garmash.viewcontrollers.splash.presenter.MainScreenPresenterImpl
import reddit.garmash.viewcontrollers.splash.view.MainScreenView
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class MainActivity : BaseActivity(), MainScreenView, MainPostAdapter.OnRedditPostClick {

    override fun onPostClick(post: RedditPost) {
        val browserIntent = Intent(Intent.ACTION_VIEW,  Uri.parse(Const.URL + post.link))
        startActivity(browserIntent)
    }


    override fun hideLoading() {
        (findViewById<View>(R.id.pb_main) as ProgressBar).visibility = View.GONE
        rvTop?.visibility = View.VISIBLE
    }

    private var rvTop: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rvTop = findViewById<View>(R.id.top_items) as RecyclerView
        MainScreenPresenterImpl(this).onCreate()
        initAdapter()
    }

    override fun initAdapter() {
        // DataSource
        val dataSource = PostsDataSource(this)


        // PagedList
        val config = PagedList.Config.Builder() // require MVVM arch
                .setEnablePlaceholders(false)
                .setPageSize(30)
                .setPrefetchDistance(10)
                .setInitialLoadSizeHint(10)
                .build()


        val pagedList = PagedList.Builder(dataSource, config)
                .setNotifyExecutor(MainThreadExecutor())
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .build()


        // Adapter
        val adapter = MainPostAdapter(this, PostDiffCallback())
        adapter.submitList(pagedList)


        // RecyclerView
        rvTop?.layoutManager = LinearLayoutManager(this)
        rvTop?.adapter = adapter

    }

    internal inner class MainThreadExecutor : Executor {
        private val mHandler = Handler(Looper.getMainLooper())

        override fun execute(command: Runnable) {
            mHandler.post(command)
        }
    }


}
