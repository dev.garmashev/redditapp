package reddit.garmash.viewcontrollers.main.models

import com.google.gson.annotations.SerializedName

/**
 * Title (at its full length)
 * Author and subreddit
 * Post date (format like “x hours ago”)
 * A thumbnail for those who have a picture
 * Current rating and number of comments
 */

class RedditPost {


    @SerializedName("id")
    val id: String? = null

    @SerializedName("title")
    val title: String? = null

    @SerializedName("thumbnail")
    val thumbnail = ""

    @SerializedName("subreddit")
    val subreddit: String? = null

    @SerializedName("author")
    val author: String? = null

    @SerializedName("permalink")
    val link: String? = null

    @SerializedName("num_comments")
    val numComments: Long = 0

    @SerializedName("score")
    val score: Long = 0

    @SerializedName("created_utc")
    val created: Long = 0


}

