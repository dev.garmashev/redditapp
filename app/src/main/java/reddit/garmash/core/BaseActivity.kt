package reddit.garmash.core

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import reddit.garmash.viewcontrollers.main.R

abstract class BaseActivity : AppCompatActivity(), BaseView {


    override val context: Context
        get() = this

    override fun onUnknownError(error: String) {
        showError(error)
    }

    override fun onTimeout() {
        showError(getString(R.string.server_error))
    }

    override fun onNetworkError() {
        showError(getString(R.string.internet_connection_error))
    }

    private fun showError(error : String){
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }
}
