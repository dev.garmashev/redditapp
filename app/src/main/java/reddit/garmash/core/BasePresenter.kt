package reddit.garmash.core

interface BasePresenter {

    fun onCreate()

    fun onPause()

    fun onDestroy()
}
