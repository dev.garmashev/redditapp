package reddit.garmash.api


import reddit.garmash.viewcontrollers.main.models.RedditResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RedditApiInterface {

    @GET("top/.json")
    fun getTop(@Query("after") nextLoadPage: String?, @Query("limit") limit: Int): io.reactivex.Observable<RedditResponse>
}
