package reddit.garmash.viewcontrollers.main.models

import com.google.gson.annotations.SerializedName

class RedditChildren {

    @SerializedName("data")
    var redditPost: RedditPost? = null
}
