package reddit.garmash.api

import okhttp3.OkHttpClient
import reddit.garmash.utils.Const
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiManager private constructor(URL: String) {
    private val okHttpClient: OkHttpClient
    private val retrofit: Retrofit
    val service: RedditApiInterface

    init {
        val builderForHttp = OkHttpClient.Builder()

        if (Const.LOG_ENABLED) {
            builderForHttp.readTimeout(10, TimeUnit.MINUTES)
            builderForHttp.writeTimeout(10, TimeUnit.MINUTES)
            builderForHttp.connectTimeout(10, TimeUnit.MINUTES)
            builderForHttp.addInterceptor(LoggingInterceptor())

        } else {
            builderForHttp.readTimeout(120, TimeUnit.SECONDS)
            builderForHttp.writeTimeout(120, TimeUnit.SECONDS)
            builderForHttp.connectTimeout(120, TimeUnit.SECONDS)
        }

        okHttpClient = builderForHttp.build()
        retrofit = Retrofit.Builder()
                .baseUrl(URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        service = retrofit.create(RedditApiInterface::class.java)
    }

    companion object {


        private var singleton: ApiManager? = null

        @Synchronized
        fun getInstance(): ApiManager {
            if (null == singleton) {
                singleton = ApiManager(Const.URL)
            }
            return singleton!!
        }
    }

}
