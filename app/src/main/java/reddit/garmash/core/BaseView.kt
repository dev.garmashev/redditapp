package reddit.garmash.core

import android.content.Context

interface BaseView {

    val context: Context

    fun onUnknownError(error : String)

    fun onTimeout()

    fun onNetworkError();


}
