package reddit.garmash.viewcontrollers.main.repository


interface RedditPostsRepository {

    fun getRedditPosts(loadSize: Int, nextLoadKey: String?, onLoadComplete: RedditPostsRepositoryImpl.OnLoadComplete)

}
