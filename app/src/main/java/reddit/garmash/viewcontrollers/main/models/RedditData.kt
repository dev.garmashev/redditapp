package reddit.garmash.viewcontrollers.main.models

import com.google.gson.annotations.SerializedName
import java.util.*

class RedditData {


    @SerializedName("children")
    var redditChildren: ArrayList<RedditChildren>? = null

    @SerializedName("after")
    val nextLoadKey: String? = null
}
