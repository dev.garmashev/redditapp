package reddit.garmash.utils

import reddit.garmash.viewcontrollers.main.BuildConfig

object Const {

    val LOG_ENABLED = BuildConfig.DEBUG
    val URL = "https://www.reddit.com/"
    val LIMIT = 20
}
