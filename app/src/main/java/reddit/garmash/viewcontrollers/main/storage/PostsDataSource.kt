package reddit.garmash.viewcontrollers.main.storage

import android.arch.paging.PageKeyedDataSource
import reddit.garmash.core.BaseView
import reddit.garmash.viewcontrollers.main.models.RedditPost
import reddit.garmash.viewcontrollers.main.repository.RedditPostsRepository
import reddit.garmash.viewcontrollers.main.repository.RedditPostsRepositoryImpl

class PostsDataSource(val baseView : BaseView) : PageKeyedDataSource<String, RedditPost>() {
    private var mRedditPostsRepository: RedditPostsRepository = RedditPostsRepositoryImpl(baseView)


    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<String, RedditPost>) {
      //no-op
    }

    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<String, RedditPost>) {
        mRedditPostsRepository.getRedditPosts(params.requestedLoadSize, null, object : RedditPostsRepositoryImpl.OnLoadComplete {
            override fun onLoadComplete(redditPosts: List<RedditPost>?, nextLoadKey: String?) {
                callback.onResult(redditPosts!!, null, nextLoadKey)
            }
        })
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, RedditPost>) {
        mRedditPostsRepository.getRedditPosts(params.requestedLoadSize, params.key, object : RedditPostsRepositoryImpl.OnLoadComplete {
            override fun onLoadComplete(redditPosts: List<RedditPost>?, nextLoadKey : String?) {
                callback.onResult(redditPosts!!, nextLoadKey)
            }
        })
    }


}
