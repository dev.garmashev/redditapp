package reddit.garmash.api

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody
import okio.Buffer
import java.io.IOException


internal class LoggingInterceptor : Interceptor {
    private val TAG = "LoggingInterceptor"

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val t1 = System.nanoTime()
        Log.e(TAG, String.format("Sending request %s", request.url()))
        val body: String
        if (request.method().compareTo("post", ignoreCase = true) == 0) {
            body = bodyToString(request)
            Log.e(TAG, String.format("Body:%s", body))

        }
        val response: Response?
        response = chain.proceed(request)


        val t2 = System.nanoTime()

        var bodyString: String?
        val timeOfExecution: Double
        try {
            bodyString = response!!.body()!!.string()
            timeOfExecution = (t2 - t1) / 1000000000.0
            Log.e(TAG, String.format("Received response for %s in %.1fs%n%s",
                    response.request().url(), timeOfExecution, bodyString))

        } catch (e: Exception) {
            e.printStackTrace()
            bodyString = ""
        }


        return response!!.newBuilder()
                .body(ResponseBody.create(response.body()!!.contentType(), bodyString!!))
                .build()
    }

    companion object {

        fun bodyToString(request: Request): String {
            return try {
                val copy = request.newBuilder().build()
                val buffer = Buffer()
                copy.body()!!.writeTo(buffer)
                buffer.readUtf8()
            } catch (e: IOException) {
                "did not work"
            }

        }
    }
}