package reddit.garmash.viewcontrollers.splash.presenter

import reddit.garmash.viewcontrollers.splash.view.SplashScreenView

class SplashScreenPresenterImpl(private val screenView: SplashScreenView) : SplashScreenPresenter {

    override fun onCreate() {
        screenView.openNextScreen()
    }

    override fun onPause() {
    }

    override fun onDestroy() {

    }
}
