package reddit.garmash.viewcontrollers.main.models

import com.google.gson.annotations.SerializedName
import java.util.*

class RedditResponse {


    @SerializedName("data")
    val redditData: RedditData? = null


    val redditPosts: List<RedditPost>
        get() {

            val redditPosts = ArrayList<RedditPost>()

            for (redditChildren in redditData?.redditChildren!!) {

                redditPosts.add(redditChildren.redditPost!!)

            }
            return redditPosts
        }

}
